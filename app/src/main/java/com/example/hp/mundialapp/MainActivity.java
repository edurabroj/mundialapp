package com.example.hp.mundialapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.hp.mundialapp.fragments.AllFragment;
import com.example.hp.mundialapp.fragments.TodayFragment;

import static com.example.hp.mundialapp.utils.Utils.showAcercaDe;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView navegacion;
    private FrameLayout mainFrame;

    private AllFragment allFragment;
    private TodayFragment todayFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navegacion = findViewById(R.id.navegacion);
        mainFrame = findViewById(R.id.mainFrame);

        allFragment= new AllFragment();
        todayFragment=new TodayFragment();

        navegacion.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_today:
                        setFragment(todayFragment);
                        return true;
                    case R.id.nav_all:
                        setFragment(allFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });

        navegacion.setSelectedItemId(R.id.nav_today);
    }

    private void setFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.mainFrame,fragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.acercaDe:
                showAcercaDe(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
