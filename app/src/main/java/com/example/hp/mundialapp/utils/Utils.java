package com.example.hp.mundialapp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Toast;

import com.example.hp.mundialapp.PartidosAdapter;
import com.example.hp.mundialapp.R;
import com.example.hp.mundialapp.entidades.Partido;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utils {
    public static void showMsg(String msg, Context context){
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    public static void cargarDatos(final View cargandoView, Call<List<Partido>> solicitud, final PartidosAdapter adaptador, final Context context, final SwipeRefreshLayout refresh) {
        cargandoView.setVisibility(View.VISIBLE);
        solicitud.enqueue(new Callback<List<Partido>>() {
            @Override
            public void onResponse(Call<List<Partido>> call, Response<List<Partido>> response) {
                cargandoView.setVisibility(View.GONE);
                refresh.setRefreshing(false);
                if(response.isSuccessful()){
                    adaptador.setDataset(response.body());
                }else{
                    showMsg("Respuesta errónea",context);
                }
            }

            @Override
            public void onFailure(Call<List<Partido>> call, Throwable t) {
                refresh.setRefreshing(false);
                cargandoView.setVisibility(View.GONE);
                showMsg("Error en la solicitud",context);
            }
        });
    }

    private static void showDialog(final Context contexto, final String title, final Spanned message) {
        new AlertDialog.Builder(contexto){{
            setTitle(title);
            setMessage(message);
            setPositiveButton(contexto.getString(R.string.acerca_de_btnAceptar), null);
        }}.create().show();
    }


    private static PackageInfo getPackageInfo(Context context)
    {
        PackageInfo pckginfo = null;
        try {
            pckginfo = context.getPackageManager().getPackageInfo
                    (context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pckginfo;
    }

    public static void showAcercaDe(final Context contexto) {
        PackageInfo pckginfo = getPackageInfo(contexto);
        assert pckginfo != null;
        String nombreVersion = pckginfo.versionName;

        String acerca=contexto.getString(R.string.menu_acercaDe);
        String nombreApp =contexto.getResources().getString(R.string.app_name);

        showDialog(
                contexto,
                acerca + " "+nombreApp,
                Html.fromHtml(
                        String.format(
                                contexto.getString(R.string.acerca_de_mensaje),
                                nombreVersion
                        )
                )
        );
    }
}
