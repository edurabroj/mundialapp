package com.example.hp.mundialapp.service;

import com.example.hp.mundialapp.entidades.Partido;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MundialService {
    @GET("matches")
    Call<List<Partido>> ObtenerPartidos();

    @GET("matches/today")
    Call<List<Partido>> ObtenerPartidosHoy();

}
