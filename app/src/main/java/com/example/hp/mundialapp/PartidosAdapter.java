package com.example.hp.mundialapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hp.mundialapp.entidades.Partido;
import com.example.hp.mundialapp.entidades.Team;

import java.util.ArrayList;
import java.util.List;

public class PartidosAdapter extends RecyclerView.Adapter<PartidosAdapter.VH> {
    private List<Partido> dataset;
    private View.OnClickListener clickListener;
    private boolean hasDate;

    public PartidosAdapter(boolean hasDate) {
        this.dataset = new ArrayList<>();
        this.hasDate = hasDate;
    }

    public void setDataset(List<Partido> dataset) {
        this.dataset = dataset;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_partido,parent,false);
        view.setOnClickListener(clickListener);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.setData(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvHomeTeamName, tvHomeTeamGoals, tvAwayTeamName, tvAwayTeamGoals,
                tvTime, tvStatus, tvStadium, tvDatetime;

        private VH(View itemView) {
            super(itemView);
            tvHomeTeamName = itemView.findViewById(R.id.homeTeamName);
            tvHomeTeamGoals = itemView.findViewById(R.id.homeTeamGoals);
            tvAwayTeamName = itemView.findViewById(R.id.awayTeamName);
            tvAwayTeamGoals= itemView.findViewById(R.id.awayTeamGoals);
            tvTime = itemView.findViewById(R.id.time);
            tvStatus = itemView.findViewById(R.id.status);
            tvStadium = itemView.findViewById(R.id.tvStadium);
            tvDatetime = itemView.findViewById(R.id.tvDatetime);
        }

        public void setData(final Partido partido) {
            Team homeTeam = partido.getHome_team(), awayTeam = partido.getAway_team();

            tvHomeTeamName.setText(homeTeam.getCountry());
            tvHomeTeamGoals.setText(homeTeam.getGoals()+"");
            tvAwayTeamName.setText(awayTeam.getCountry());
            tvAwayTeamGoals.setText(awayTeam.getGoals()+"");
            tvTime.setText(partido.getTime());
            tvStatus.setText(partido.getStatus());
            tvStadium.setText(partido.getLocation());

            if(!hasDate){
                tvDatetime.setVisibility(View.GONE);
            }else {
                String[] parts = partido.getDatetime().split("T");
                if(parts.length>0){
                    tvDatetime.setText(parts[0]);
                }else {
                    tvDatetime.setText(partido.getDatetime());
                }
            }
        }
    }

    public void setOnClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
    }
}