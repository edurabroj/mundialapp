package com.example.hp.mundialapp.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MundialServiceProvider {
    public static MundialService getService(){
        return new Retrofit.Builder()
                .baseUrl("http://worldcup.sfg.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MundialService.class);
    }
}


//    Retrofit retrofit = new Retrofit.Builder()
//            .baseUrl("http://worldcup.sfg.io")
//            .addConverterFactory(GsonConverterFactory.create())
//            .build();
//        return retrofit.create(MundialService.class);
