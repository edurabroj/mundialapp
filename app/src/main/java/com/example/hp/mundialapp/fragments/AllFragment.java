package com.example.hp.mundialapp.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.hp.mundialapp.PartidosAdapter;
import com.example.hp.mundialapp.R;
import com.example.hp.mundialapp.service.MundialService;
import com.example.hp.mundialapp.service.MundialServiceProvider;

import static com.example.hp.mundialapp.utils.Utils.cargarDatos;


public class AllFragment extends Fragment {
    MundialService service;
    RecyclerView recyclerView;
    PartidosAdapter adapter;
    ProgressBar isLoading;
    SwipeRefreshLayout refresh;

    public AllFragment() {
        adapter = new PartidosAdapter(true);
        service = MundialServiceProvider.getService();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_partidos_list,container,false);

        isLoading = view.findViewById(R.id.isLoading);
        refresh = view.findViewById(R.id.refresh);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        cargarData();

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarData();
            }
        });

        return view;
    }

    private void cargarData() {
        cargarDatos(isLoading,service.ObtenerPartidos(),adapter,getContext(),refresh);
    }
}
